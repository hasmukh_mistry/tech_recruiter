<?php
/*
Template Name: Foundation page
*/
?>

<?php get_header(); ?>
	
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main">
				
				<h1>Foundation Demo</h1>

                <div data-success-message class="callout success">
                    <h2>Some Fondation fun!</h2>
                </div>	

                <div class="clearfix">
                    <ul class="dropdown menu float-left" data-dropdown-menu id="primary-menu">
                        <li>
                            <a href="#">Item 1</a>
                            <ul class="menu">
                                <li><a href="#">Item 1A</a></li>
                                <li>
                                    <a href="#">Item 1B</a>
                                    <ul class="menu">
                                        <li><a href="#">Item 1B i</a></li>
                                        <li><a href="#">Item 1B ii</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Item 1C</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">Item 2</a>
                            <ul class="menu">
                                <li><a href="#">Item 2A</a></li>
                                <li><a href="#">Item 2B</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Item 3</a></li>
                        <li><a href="#">Item 4</a></li>                    
                    </ul>                    
                    <ul class="dropdown menu float-left">
                        <li class="row">
                            <a href="#" class="large-8 medium-8 small-12 columns" style="background-color:#efefef;padding:20px;border:1px solid #ccc;margin-left:25px;" data-open="mobile-ios-modal-2">Click me for a modal</a>
                        </li>
                    </ul>
                </div>    
                

                <div class="reveal mobile-ios-modal" id="mobile-ios-modal-2" data-reveal>
                    <div class="mobile-ios-modal-inner">
                        <h1 class="float-left">Awesome. I have it.</h1> <button data-close class="float-right">X</button>
                        <div class="clearfix"></div>
                        <p>Your couch. It is mine.</p>
                        <p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
                    </div>
                </div>
                <br /><br /><br /><br /><br /><br /><br />
			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->
<?php get_footer(); ?>